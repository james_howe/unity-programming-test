﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public UnityEvent PowerupCollected = new UnityEvent();
    public float movementSpeed = 6.0f;

    // Start is called before the first frame update
    public void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    public void Update()
    {
        // Move paddle left or right
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-movementSpeed, 0);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementSpeed, 0);
        }
    }

    public void OnCollisionEnter2D(Collision2D coll) 
    {
        switch (coll.gameObject.tag)
        {
            case "PowerUp":
                PowerupCollected.Invoke();
                Destroy(coll.gameObject);
                break;
            default:
                break;
        }
    }
}
