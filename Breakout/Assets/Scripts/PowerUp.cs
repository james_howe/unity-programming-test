﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float fallSpeed = 2.0f;

    // Start is called before the first frame update
    public void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1.0f) * fallSpeed;
    }

    public void OnCollisionEnter2D(Collision2D coll) 
    {
        switch (coll.gameObject.tag)
        {
            case "Bottom":
                // Power Up was missed
                Destroy(gameObject);
                break;
            default:
                break;
        }
    }
}
