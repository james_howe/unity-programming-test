﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ball : MonoBehaviour
{
    public float movementSpeed = 6.0f;
    public UnityEvent BallLost = new UnityEvent();

    // Start is called before the first frame update
    public void Start()
    {
        Vector3 initialDirection = new Vector2(Random.Range(-1.0f, 1.0f), 1).normalized;
        GetComponent<Rigidbody2D>().velocity = initialDirection * movementSpeed;
    }

    public void OnCollisionEnter2D(Collision2D coll) 
    {
        switch (coll.gameObject.tag)
        {
            case "Bottom":
                // Ball has fallen through the bottom of the screen
                Destroy(gameObject);
                BallLost.Invoke();
                break;
            case "Paddle":
                // change ball direction depending on where on paddle it hits
                float hitPosition = (transform.localPosition.x - coll.transform.localPosition.x) / coll.collider.bounds.size.x;
                Vector2 dir = new Vector2(hitPosition, 1).normalized;
                GetComponent<Rigidbody2D>().velocity = dir * movementSpeed;
                break;
            default:
                break;
        }
    }
}
