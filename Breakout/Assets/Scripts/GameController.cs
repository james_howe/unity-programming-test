﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


public class GameController : MonoBehaviour
{
    public int Lives = 3;
    
    public int rows = 5;
    public int columns = 9;

    public int level = 1;
    public float levelSpeedUp = 0.5f;

    public float blockGap = 0.05f;

    [SerializeField] private Text LifeCounter = default;
    [SerializeField] private Text GameText = default;
    [SerializeField] private GameObject ballPrefab = default;
    [SerializeField] private GameObject blockPrefab = default;
    [SerializeField] private PlayerController playerPaddle = default;
    
    [SerializeField] private Transform leftWall = default;
    [SerializeField] private Transform rightWall = default;
    [SerializeField] private Transform topWall = default;

    private float blockWidth;
    private float blockHeight;
    private Ball ball;

    private int nActiveBalls = 0;
    private int nUnbreakables = 0;

    private bool spawnBall = false;
    private bool gameStarted = false;
    
    private List<Color> colourPattern = new List<Color>(){Color.red, Color.blue, Color.green, Color.yellow, Color.magenta};
    private int patternLength = 0;

    public void Start()
    {
        GameText.text = "Press Space to start";
        GameText.gameObject.SetActive(true);
        patternLength = colourPattern.Count;

        playerPaddle.PowerupCollected.AddListener(onPowerupCollected);
    }

    public void StartGame()
    {
        spawnBall = true;
        Lives = 3;
        LifeCounter.text = string.Format("Level: {0}\nLives: {1}", level, Lives);

        // Get the block dimensions for positioning calculations
        Block block = Instantiate(blockPrefab).GetComponent<Block>();
        blockWidth = block.width;
        blockHeight = block.height;
        Destroy(block.gameObject);

        // Calculate the offset the blocks need to be centered between the two walls
        float wallOffset = (blockWidth + rightWall.localPosition.x - leftWall.localPosition.x - (columns * blockWidth) - ((columns - 1) * blockGap)) / 2.0f;

        // set up blocks
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                block = Instantiate(blockPrefab).GetComponent<Block>();

                float xPos = leftWall.localPosition.x + wallOffset + ((blockWidth + blockGap) * i);
                float yPos = topWall.localPosition.y - 2 * blockHeight - ((blockHeight + blockGap) * j);

                block.gameObject.transform.localPosition = new Vector3(xPos, yPos, 0);
                block.ChangeColour(colourPattern[j % patternLength]);

                if (Random.Range(1, 11) == 1)
                {
                    // 10% chance for a block to be unbreakable (note can possibly cause a softlock)
                    block.SetBlockType(Block.BlockType.Unbreakable);
                    nUnbreakables += 1;
                }
                else if (Random.Range(1, 21) == 1)
                {
                    // 5% chance for a block to contain a powerup
                    block.SetBlockType(Block.BlockType.Powerup);
                }
            }
        }

        GameText.gameObject.SetActive(false);
        gameStarted = true;
    }

    // Update is called once per frame
    public void Update()
    {
        if (gameStarted)
        {
            if (spawnBall)
            {
                Debug.Log("New ball!");
                spawnBall = false;
                ball = Instantiate(ballPrefab).GetComponent<Ball>();
                nActiveBalls += 1;
                ball.movementSpeed += levelSpeedUp * level;
                ball.BallLost.AddListener(onBallLost);
            }

            if (GameObject.FindGameObjectsWithTag("Block").Length == nUnbreakables)
            {
                // No more blocks left -> level won
                GameText.text = "Level up! \nPress Space to start";
                GameText.gameObject.SetActive(true);

                ClearBoard(); // To remove remaining unbreakable blocks

                // Increase level
                level += 1;
                gameStarted = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Start game");
                ClearBoard();
                StartGame();
            }
        }
    }

    private void onBallLost()
    {
        nActiveBalls -= 1;
        if (nActiveBalls <= 0)
        {
            Debug.Log("no ball");
            // No balls left 
            Lives -= 1;
            LifeCounter.text = string.Format("Level: {0}\nLives: {1}", level, Lives);

            if (Lives > 0)
            {
                spawnBall = true;
            }
            else
            {
                Debug.Log("Game over!");
                GameText.text = "Game over! \nPress Space to restart";
                GameText.gameObject.SetActive(true);
                gameStarted = false;
                level = 1;
            }
        }
    }

    private void onPowerupCollected()
    {
        // Just give spawn a bonus ball, can be changed to have different powerup types another time
        spawnBall = true;
    }

    private void ClearBoard()
    {
        GameObject[] remainingBlocks = GameObject.FindGameObjectsWithTag("Block");
        foreach (GameObject block in remainingBlocks)
        {
            Destroy(block);
        }
        nUnbreakables = 0;

        GameObject[] powerups = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach (GameObject powerup in powerups)
        {
            Destroy(powerup);
        }

        GameObject[] extraBalls = GameObject.FindGameObjectsWithTag("Ball");
        foreach (GameObject extraBall in extraBalls)
        {
            Destroy(extraBall);
        }
    }
}
