﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public enum BlockType {Regular, Unbreakable, Powerup};

    [HideInInspector]
    public float height;
    [HideInInspector]
    public float width;

    [SerializeField] private GameObject powerUpPrefab = default;

    public bool Unbreakable = false;
    public bool SpawnPowerup = false;

    public void Awake()
    {
        height = gameObject.GetComponent<BoxCollider2D>().bounds.size.y;
        width = gameObject.GetComponent<BoxCollider2D>().bounds.size.x;
    } 

    public void SetBlockType(BlockType type)
    {
        switch (type)
        {
            case BlockType.Regular:
                SpawnPowerup = false;
                Unbreakable = false;
                break;
            case BlockType.Unbreakable:
                Unbreakable = true;
                SpawnPowerup = false;
                ChangeColour(Color.grey);
                break;
            case BlockType.Powerup:
                SpawnPowerup = true;
                Unbreakable = false;
                break;
            default:
                Debug.Log("Invalid block type");
                break;
        }
    }

    public void OnCollisionEnter2D(Collision2D coll) 
    {
        if (coll.gameObject.tag == "Ball" && !Unbreakable) 
        {
            if(SpawnPowerup)
            {
                // Spawn a powerup
                Instantiate(powerUpPrefab, gameObject.transform.localPosition, Quaternion.identity);
            }
            Destroy(gameObject);
        }
     }
     
    public void ChangeColour(Color colour)
    {
        gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", colour);
    }
}
