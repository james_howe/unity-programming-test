# README #

## Instructions ##

Unity version 2019.3.0f6 was used. It is playable from the editor, press space to start and move left and right with the left and right arrows.
Clearing the screen resets the game but with a faster ball, until you run out of lives and it resets.

### Breakout ###
We are going old school and want to test your skills at creating a Breakout game.  There are a number of breakout style games available on the internet for you to test out.
Don't spend too long on this. 3-4 hours is more than enough.

### Functional Requirements ###
1. The ball should rebound off the sides, top of the game board and off the paddle. 
2. If the ball falls off the bottom of the board the game is over or a life is lost.
3. The game should play from the Unity Editor.
4. The game should be re-playable without closing the game.

### What we're looking for ###
* The process you used to complete this test
* Prudent use of Object Orientated design
* Code reusability
* Extensibility and maintainability of the software
* Use of appropriate Unity capabilities
* Use of best practises
* Your creativity in making the game fun

### Deliverables ###
* Instructions on how to run and play your game
* Code should be committed to a git repository hosted on [bitbucket.org](https://bitbucket.org)

### Extra Credit ###
* Can be played in both orientations; landscape and portrait
* Can be installed as an app
* Any extra features you want to include in the game

## Technology ##
You can use assets (excluding code) from the Unity Asset Store, however please note what assets you have used and why you have used them.

## How do I get set up? ##
* [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
* Create a new Unity project in the repository. You should use a recent version of Unity (2019.3 or later).

## Next Steps ##
After you have finished your submission, make sure the reviewers have read access to your repository. Our developers will review your submission and then invite you in for a discussion.